<!DOCTYPE html>
<html>
<head>
<meta name=viewport content="width=device-width, initial-scale=1">
<title>virtualhacker.net</title>
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<div id="header">
	<div id="titlebox"><a href="index.php"><div class="logo"></div></a></div>
</div>
<div id="wrapper">
<?php
require_once 'Parsedown.php';

function pagnation($data0, $data, $data2, $data3){
	echo '<div class="pagnation">';
		echo "<div id='newleft'>";
			if ($data0 < $data){
				echo "<span class='pages'><a href='/?p=blog&index=" . $data2 . "'>Newer Posts</a></span>";
			}
		echo "</div>";
		echo "<div id='oldright'>";
			if ($data0 > 10){
				echo "<span class='pages'><a href='/?p=blog&index=" . $data3 . "'>Older Posts</a></span>";
			}
				// else{
			// 	echo "<span class='pagesnot'>Older Posts</span>";
			// }
		echo "</div>";
	echo "</div>";
}
function blog(){
	$count = 0;
	$images_find = array();
	foreach (glob("posts/*.md") as $filename) {
		$images_find[] = $filename;
		$count++;
	}
	$count += 1;
	$index = $count;
	$check = $_GET['index'];
	if (isset($check)){
		$index = strip_tags($_GET['index']);
	}
	if ($index > 20){
		$next = $index - 20;
	}else{
		$next = 20;
	}
	if ($index < $count){
		$prev = $index + 20;
	}else if($index == $count){
		$prev = $count;
	}
	$menu = $index;
	pagnation($menu, $count, $prev, $next);

    $total = $index - 20;
    if ($index > 0){
            while ($index > $total){
                    if (isset($images_find[$index])){
                            if (file_exists("posts/" . $index . ".md") == "True") {
                                    $crap = file_get_contents("posts/" . $index . ".md");
                                    $file_array = explode("\n",$crap);
                                    echo  '<div class="linklist"><a href="/?p=' . $index . '">' . str_replace("# ","",$file_array[0]) . '</a></div>'. '<div class="date">' . str_replace("_","",$file_array[1]) . '</div>';

                            }
                    }
            $index--;
            }
    }
	pagnation($menu, $count, $prev, $next);
	}
	?>
<?php
function article_pagnation(){
	$count = -1;
	$images_find = array();
	foreach (glob("posts/*.md") as $filename) {
		$images_find[] = $filename;
		$count++;
	}
	echo '<div class="pagnation">';
	echo "<div id='newleft'>";
	if (((int)$_GET['p'] - 1) >= 0){
	echo "<span class='pages'><a href='/?p=" . ((int)$_GET['p'] - 1) . "'>Prev</a></span>";
	}
	echo "</div>";
	echo "<div id='oldright'>";
	if (((int)$_GET['p'] + 1) <= $count){
	echo "<span class='pages'><a href='/?p=" . ((int)$_GET['p'] + 1) . "'>Next</a></span>";
	}
	echo "</div>";
	echo "</div>";
}
?>

<?php
if ($_GET['p'] === "blog"){
	echo '<div id="blog">';
	blog();
	echo '</div>';
}elseif (isset($_GET['p'])){
	echo '<div id="contain">';
	article_pagnation();
	echo '<div id="singlepage">';
	echo Parsedown::instance()->text(file_get_contents("posts/" . $_GET['p'] . ".md"));
	echo '</div>';
	article_pagnation();
	echo '</div>';

}elseif (isset($_GET['pages'])){
	echo '<div id="contain">';
	echo '<div id="page">';
	echo Parsedown::instance()->text(file_get_contents("pages/" . $_GET['pages'] . ".md"));
	echo '</div>';
	echo '</div>';
}else{
	echo  '<div id="frontpage">' . Parsedown::instance()->text(file_get_contents("frontpage.md"));
	echo '</div>';
}
?>
	<div id="sidebar">
		<?php echo Parsedown::instance()->text(file_get_contents("sidebar.md")); ?>
	</div>
</div>
<div class="clear"></div>
<!-- 8894434 -->
</body>
</html>
