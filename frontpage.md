# Welcome to virtualhacker.net

We are a collection of nerds that decided it was to hard to look around for tools to use while we collaborate on different projects.
virtualhacker.net has a set of services provided that should make collaboration easier and can be found [here](http://virtualhacker.net/?pages=services).

If there are any gaps please feel free to email suggestions to admin [at] virtualhacker.net.

If there is a desire to reach us we can be found on irc, irc.virtualhacker.net on the channel #virtualhacker.
For SSL use port 6697.
There is support for SASL if so desired.

If whitelisting for playing minecraft a visit to the irc channel is mandatory and non negotiable.

Furthermore all our services are also available via cjdns and more info on the public node can be found [here](http://virtualhacker.net/?pages=services#cjdns).
