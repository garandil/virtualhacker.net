# Submitting articles To Virtualhacker.net

This information is for anyone planning to submit articles to our blog.  
All articles must be submited as .md(markdown).  

All articles/images must be sent to __articles@virtualhacker.net__  

## You must follow these formating conditions for your article:  

__The first line must be an H1 header and the second line must be the date in this format:__  

    _2016-12-28_ 

you must stick strictly to this format to save us time of having to reformat it for you.  

__Any images that come with the article must be submitted in the following format with a random number at the start and the date in the middle and your author name at the end:__  

    326-2016-09-28-yakamo.jpg/.png

__when inserting images into your post they will be epected in a specific folder:__  

    ![myimage](images/326-2016-09-28-yakamo.jpg)

__Finally you need to say who you are at the bottom as follows:__  

    _author: yakamo_

## Example article

    # Article title
    _2016-12-26_
    
    This is where the main body of text is expected and images and so on.
    ![myimage](images/326-2016-09-28-yakamo.jpg)
    
    
    _author: yakamo_
