# Services

[IRC](#irc)  
[Termbin](#termbin)  
[Cjdns](#cjdns)  
[Minecraft](#minecraft)  
[Zerobin](http://zerobin.virtualhacker.net/) - [via cjdns](http://h.zerobin.virtualhacker.net/)  
[Etherpad](http://etherpad.virtualhacker.net/)  
[Mumble](#mumble)  


## IRC - Internet Relay Chat<a name="irc"></a>  
You can join us on irc.
We encourage that you use __SSL__ when you connect.  

    ipv4/6 - irc.virtualhacker.net 
    cjdns - h.virtualhacker.net

## Termbin on the open net.<a name="termbin"></a>  
To use termbin all you have to do is:  

    cat <somefile> | nc vhbin.net 9999  

__For Cjdns access:__  

     cat <somefile> | nc h.vhbin.net 9998

There are many uses for this and ranges from sharing config files to text files.


It can also be added as an alias in .bashrc:  

    alias tb='nc vhbin.net 9999'  

__Once again the cjdns version:__  

    alias tb='nc h.vhbin.net 9998'

And with that you can fully utilize the termbin service from virtualhacker.net   

## CJDNS Public Node<a name="cjdns"></a>  
    "163.172.144.87:64281":
    {
    "login": "cjdns-public",
    "password":"3fb34v5t03jj1xtwx96bj1t5t69x12x",
    "publicKey":"pk12lx76mld8smjhu9hkqkdcjjb51nk4192b4lk4y6dxwr3qj0c0.k",
    "peerName":"virtualhacker"
    }

We also highly recommend Joining [these](http://web.transatlantic.h-ic.eu/) nodes as well, the more the better!  

## Minecraft<a name="minecraft"></a>  
Our server is whitelisted so you will need to contact either garandil or yakamo in irc to get whitelisted, we may require you to hang around and get to know us first, as we would like to keep up a good atmosphere for players.    

__Normal address:__  

    mc.virtualhacker.net

__Cjdns address:__  

    h.mc.virtualhacker.net  

We also provide a [compressed file](/mc-map-daily.tar.xz) of the world which is updated daily at 6am.  

## Mumble<a name="mumble"></a>  
Mumble is our voice chat server for anyone who wants to chat to each other or groups.

__Server:__  

    ipv4/6 - virtualhacker.net:64738  
    cjdns - h.virtualhacker.net:64738

To login choose any username you want and use the password = __VirtualHacker__   <----casesensitive
